import { Athelete } from '../models/athelete';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-athelete',
  templateUrl: './athelete.component.html',
  styleUrls: ['./athelete.component.css']
})
export class AtheleteComponent implements OnInit {

  @Input()athelete:Athelete

  constructor() { }

  ngOnInit() {
  }

}
