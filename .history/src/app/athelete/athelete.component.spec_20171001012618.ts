import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteComponent } from './athelete.component';

describe('AtheleteComponent', () => {
  let component: AtheleteComponent;
  let fixture: ComponentFixture<AtheleteComponent>;



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtheleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
