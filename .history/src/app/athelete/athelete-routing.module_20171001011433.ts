
import { NgModule } from '@angular/core';
import { AtheleteComponent } from './athelete.component';
import { RouterModule,Routes } from '@angular/router'

export const AtheleteRoutes:Routes=[
       {path:'athlete/:id',component:AtheleteComponent}

]

@NgModule({
  imports: [
    RouterModule.forChild(AtheleteRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AtheleteRoutingModule { }


