import { AtheleteRoutingModule } from './athelete-routing.module';
import { AtheleteRoutes } from './athelete.routes';
import { RouterModule } from '@angular/router';
import { AtheleteComponent } from './athelete.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
     AtheleteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
     AtheleteRoutingModule
  ],
  exports:[
    AtheleteComponent
  ]

})
export class AtheleteModule { }
