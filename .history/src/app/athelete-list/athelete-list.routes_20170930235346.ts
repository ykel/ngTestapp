import { AtheleteComponent } from '../athelete/athelete.component';
import { AtheleteListComponent } from './athelete-list.component';

import { Routes } from '@angular/router'

export const AtheletesListRoutes:Routes=[
       {path:'athletes-list',component:AtheleteListComponent},
          {path:'athlete/:id',component:AtheleteComponent}

]

