import { AtheleteListComponent } from './athelete-list.component';

import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AtheletesListRoutes} from 'app/athelete-list/athelete-list.routes';

@NgModule({
  declarations: [
     AtheleteListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
      RouterModule.forRoot(
      AtheletesListRoutes
    )
  ],

})
export class AtheleteListModule { }
