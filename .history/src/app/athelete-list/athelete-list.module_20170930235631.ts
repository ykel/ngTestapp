import { AtheleteModule } from '../athelete/athelete.module';
//import { AtheleteComponent } from '../athelete/athelete.component';
import { AtheleteListComponent } from './athelete-list.component';

import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AtheletesListRoutes} from 'app/athelete-list/athelete-list.routes';

@NgModule({
  declarations: [
     AtheleteListComponent,
   //  AtheleteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AtheleteModule,
    ReactiveFormsModule,
      RouterModule.forRoot(
      AtheletesListRoutes
    )
  ],

})
export class AtheleteListModule { }
