// import { TestBed, async } from '@angular/core/testing';

// import { AppComponent } from './app.component';

// describe('AppComponent', () => {
//   beforeEach(async(() => {
//     //fake angular module where we want to place our
//     TestBed.configureTestingModule({
//       declarations: [
//         AppComponent
//       ],
//     }).compileComponents();
//   }));

//    //checks if the component is created
//   it('should create the app', async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app).toBeTruthy();
//   }));

//    //checks if there is a variable called title equals to app works
//   it(`should have as title 'app works!'`, async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app.title).toEqual('My Friends');
//   }));

//    //checks if the variable is shown in h1 tag
//   it('should render title in a h1 tag', async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     fixture.detectChanges();
//     const compiled = fixture.debugElement.nativeElement;
//     expect(compiled.querySelector('h1').textContent).toContain('My Friends');
//   }));

//   it('should contain a button which increments a value by 1', async()=>{
//          const fixture = TestBed.createComponent(AppComponent);
//          //ensure changes in the view are checked
//          fixture.detectChanges()
//          expect(fixture.componentInstance.points).toBe(1)
//          fixture.debugElement.nativeElement.querySelector('button').click();
//          expect(fixture.componentInstance.points).toBe(2)
//   })
// });
