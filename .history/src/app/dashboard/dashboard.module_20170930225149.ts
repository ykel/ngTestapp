import { DashboardComponent } from './dashboard.component';
import { DashboardComponentRoutes } from './dashboard.routes';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
   DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
      RouterModule.forRoot(
         DashboardComponentRoutes
    )
  ],

})
export class AtheleteModule { }
