import { appRoutes } from './routes';
import { AtheleteModule } from './athelete/athelete.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AtheleteModule,
   RouterModule.forRoot(
       appRoutes
   )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
