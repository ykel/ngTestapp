import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'email-header',
  templateUrl: './email-header.component.html',
  styleUrls: ['./email-header.component.css']
})
export class EmailHeaderComponent implements OnInit,OnChanges {
  
  @Input()sender:string
  @Input()receiver:string
  @Input()subject:string
  form:FormGroup
  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {

    this.initForm(this.sender,this.receiver,this.subject)
  }

  ngOnChanges(){
    this.initForm(this.sender,this.receiver,this.subject)
  }


  initForm(sender:string,receiver:string,subject:string){
       this.form=this.formBuilder.group({
            sender:[sender],
            receiver:[receiver],
            subject:[subject]
       })
  }

}
