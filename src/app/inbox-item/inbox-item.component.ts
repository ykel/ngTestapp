import { Component, OnInit, Input } from '@angular/core';
import { Email } from "app/models/email";

@Component({
  selector: 'inbox-item',
  templateUrl: './inbox-item.component.html',
  styleUrls: ['./inbox-item.component.css']
})
export class InboxItemComponent implements OnInit {

   @Input()email:Email

  constructor() { }

  ngOnInit() {
  }

}
