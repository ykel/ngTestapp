import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router'
export const appRoutes:Routes=[

       {
           path:'dashboard',
           component:DashboardComponent
       },

       {
           path:'',
           redirectTo:'/dashboard',
           pathMatch:'full'
       }
]

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class  AppRoutingModule { }





