import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'email-body',
  templateUrl: './email-body.component.html',
  styleUrls: ['./email-body.component.css']
})
export class EmailBodyComponent implements OnInit {
  @Input()body:string
  form:FormGroup
  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {
     this.initForm(this.body)
  }

  ngOnChanges(){
       this.initForm(this.body)
  }


  initForm(body:string){
       this.form=this.formBuilder.group({
          body:[body]
       })
  }

}
