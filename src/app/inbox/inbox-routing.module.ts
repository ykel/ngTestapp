
import { NgModule } from '@angular/core';

import { RouterModule,Routes } from '@angular/router'
import { InboxComponent } from "app/inbox/inbox.component";

export const InboxRoutes:Routes=[
       {path:'inbox',component:InboxComponent}

]

@NgModule({
  imports: [
    RouterModule.forChild(InboxRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class InboxRoutingModule { }


