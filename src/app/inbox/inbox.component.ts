import { Component, OnInit } from '@angular/core';
import { Email } from "app/models/email";
import { EmailService } from "app/service/email.service";

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  emails:Array<Email>

  constructor(private emailService:EmailService) { }

  async ngOnInit() {
     
    this.emails=await this.emailService.getInbox() as Array<Email>
     
  }

}
