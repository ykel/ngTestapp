
import { RouterModule } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { InboxRoutingModule } from "app/inbox/inbox-routing.module";
import { InboxComponent } from "app/inbox/inbox.component";
import { InboxItemComponent } from "app/inbox-item/inbox-item.component";


@NgModule({
  declarations: [
    InboxComponent,
    InboxItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InboxRoutingModule
 
  ],
  exports:[
   
  ]

})
export class InboxModule { }
