

import { Injectable } from "@angular/core";
import { Email } from "app/models/email";



@Injectable()
export class EmailService{


    async getInbox(){

          var promise=new Promise((resolve,reject)=>{
                 let emails=this.loadEmails()
                 if(emails instanceof Array){
                     resolve(emails)
                 }
                 else{
                        reject(Error("some error occured"))
                 }
          })
          return promise
           
    }

    async getById(id:number){
          let searchedEmail:Email
          let emails=await this.getInbox() as Array<Email>;
          emails.some(email=>{
   
             if(id!==undefined && email.id==id){
                searchedEmail=email
                return true
             }
                 
          })
          return searchedEmail
           
    }


    private loadEmails():Array<Email>{
       let emails:Array<Email>=new Array<Email>(
            new Email("john@gmail.com","mo@gmail.com","hello","how are you?",1),
            new Email("yusuf@gmail.com","mo@gmail.com","hello","how are you?",2),
            new Email("toni@gmail.com","mo@gmail.com","hello","how are you?",3),
        )

        return emails
      
    }

}