import { AtheleteListModule } from './athelete-list/athelete-list.module';
import { DashboardModule } from './dashboard/dashboard.module';
import {appRoutes, AppRoutingModule} from './routes';
import {BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { InboxComponent } from './inbox/inbox.component';
import { InboxItemComponent } from './inbox-item/inbox-item.component';
import { EmailComponent } from './email/email.component';
import { EmailHeaderComponent } from './email-header/email-header.component';
import { EmailBodyComponent } from './email-body/email-body.component';
import { InboxModule } from "app/inbox/inbox.module";
import { EmailModule } from "app/email/email.module";
import { EmailService } from "app/service/email.service";

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AtheleteListModule,
    DashboardModule,
    AppRoutingModule,
    InboxModule,
    EmailModule
  ],
  providers: [
    EmailService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
