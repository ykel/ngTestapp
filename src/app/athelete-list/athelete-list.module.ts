import { AtheleteModule } from '../athelete/athelete.module';

import { AtheleteListComponent } from './athelete-list.component';

import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AtheleteListRoutingModule} from 'app/athelete-list/athelete-list-routing.module';

@NgModule({
  declarations: [
     AtheleteListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AtheleteModule,
    ReactiveFormsModule,
   AtheleteListRoutingModule
  ]

})
export class AtheleteListModule { }
