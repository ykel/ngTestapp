
import { NgModule } from '@angular/core';

import { AtheleteListComponent } from './athelete-list.component';
import { RouterModule,Routes } from '@angular/router'

export const AtheletesListRoutes:Routes=[
       {path:'athletes-list',component:AtheleteListComponent}

]

@NgModule({
  imports: [
    RouterModule.forChild(AtheletesListRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AtheleteListRoutingModule { }




