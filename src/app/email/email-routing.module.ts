
import { NgModule } from '@angular/core';

import { RouterModule,Routes } from '@angular/router'
import { EmailComponent } from "app/email/email.component";


export const EmailRoutes:Routes=[
       {path:'email/:id',component:EmailComponent}

]

@NgModule({
  imports: [
    RouterModule.forChild(EmailRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmailRoutingModule { }


