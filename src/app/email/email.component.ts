import { Component, OnInit } from '@angular/core';
import { Email } from "app/models/email";
import { ActivatedRoute } from "@angular/router";
import { EmailService } from "app/service/email.service";

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  email:Email
  constructor(private route:ActivatedRoute,private emailService:EmailService) {
       
        
       
   }

  async ngOnInit() {
    const id=this.getId(this.route)
    this.email=await this.emailService.getById(parseInt(id)) as Email
    console.log('route',this.email)
  }

  private getId(route:ActivatedRoute){
        if(route.params && route.params['value'] && route.params['value']['id'] !==undefined){
             return route.params['value']['id']
        }
  }

}
