
import { RouterModule } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { EmailComponent } from "app/email/email.component";
import { EmailBodyComponent } from "app/email-body/email-body.component";
import { EmailHeaderComponent } from "app/email-header/email-header.component";
import { EmailRoutingModule } from "app/email/email-routing.module";



@NgModule({
  declarations: [
    EmailComponent,
    EmailBodyComponent,
    EmailHeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    EmailRoutingModule
  ],
  exports:[
   
  ]

})
export class EmailModule { }
